{**
 DelphiPI (Delphi Package Installer)
 Author  : ibrahim dursun (t-hex) thex [at] thexpot ((dot)) net
 License : GNU General Public License 2.0
**}
unit PackageCompiler;

interface
uses PackageInfo, PackageList, SysUtils, Classes, CompilationData, ProgressMonitor,JclIDEUtils;
  //JclBorlandTools,
type

   TPackageCompiler = class
   private
     fCompilationData: TCompilationData;
     fCancel: boolean;
     fSourceFilePaths: TStringList;
     fExtraOptions: String;
     fAllPaths: TStringList;
     FEnvPaths: TStringList;

     function ConvertToShortPaths(const paths : TStringList): string;
     function GetInstallation: TJclBDSInstallation;
     function GetPackageList: TPackageList;
     procedure AddFileToDCUPath;
     function SetEnvPath: boolean;
     function GetEnvPath: boolean;
    procedure Compiling;
    procedure Installing;
   protected
     procedure PrepareExtraOptions; virtual;
     procedure ResolveHelpFiles;
     procedure ResolveSourcePaths; virtual;
     function CompilePackage(const Info : TPackageInfo): Boolean; virtual;
     function ReCompilePackage(const Info: TPackageInfo):boolean; virtual;
     function InstallPackage(const Info : TPackageInfo;
                addFlag:boolean): Boolean; virtual;

     function AddEnvPath(const path:string):boolean;

     property Installation: TJclBDSInstallation read GetInstallation;
     property PackageList: TPackageList read GetPackageList;
     property Data: TCompilationData read fCompilationData;
   public
     constructor Create(const compilationData: TCompilationData); virtual;
     destructor Destroy; override;
     procedure Compile; virtual;
     //Properties
     property Cancel: boolean read fCancel write fCancel;
     property SourceFilePaths: TStringList read fSourceFilePaths write fSourceFilePaths;
     property AllPaths: TStringList read fAllPaths;
     property ExtraOptions: string read fExtraOptions;
     property EnvPaths:TStringList read FEnvPaths;
   end;

implementation

uses JclFileUtils, JclStrings,jclShell,JclSysInfo,Windows,Utils,JclRegistry;
{ TPackageCompiler }
const
  cEnvironment = 'Environment';
  cEnvKey='Path';

var
 IncludFiles,ExtCludFiles:TStringList;

function Excluded(const ext:string):boolean;
begin
 result:=ExtCludFiles.IndexOf(ext)>=0;
end;

function FileMatch(const ext:string): Boolean;
begin
  result:=IncludFiles.IndexOf(ext)>=0;
end;

function TPackageCompiler.GetEnvPath:boolean;
var
 Path:string;
begin
  FEnvPaths.Text:='';
  Path:=RegReadStringDef(HKEY_CURRENT_USER, cEnvironment,cEnvKey,'');
  result:=Path<>'';
  if not result then exit;
  ExpandEnvironmentVar(Path);
  FEnvPaths.Text:=StringReplace(Path,';',#13#10,[rfReplaceAll])
end;

function  TPackageCompiler.SetEnvPath:boolean;
var
 Path:string;
begin
  Path:=StringReplace(FEnvPaths.Text,#13#10,';',[rfReplaceAll]);
  SetGlobalEnvironmentVariable(cEnvKey,Path);
  result:=GetEnvPath;
end;

constructor TPackageCompiler.Create(const compilationData: TCompilationData);
var
 Path:string;
 reWriteEnv:boolean;
 i : Integer;
begin
  fCompilationData := compilationData;
  fSourceFilePaths := TStringList.Create;
  fAllPaths := TStringList.Create;
  fAllPaths.Delimiter := ';';

  FEnvPaths := TStringList.Create;
  FEnvPaths.Delimiter := ';';
  FEnvPaths.Duplicates:= dupIgnore;
  FEnvPaths.Text:='';
  if not GetEnvPath then exit;
  reWriteEnv:=false;
  i:=0;
  while i < EnvPaths.Count do begin
   Path:=EnvPaths[i];
   if DirectoryExists(Path)
    then inc(i)
    else begin
     FEnvPaths.Delete(i);
     reWriteEnv:=true;
    end;
  end;
  if not reWriteEnv then exit;
  SetEnvPath;
end;

destructor TPackageCompiler.Destroy;
begin
  fSourceFilePaths.Free;
  fAllPaths.Free;
  FEnvPaths.Free;
  inherited;
end;

function TPackageCompiler.GetInstallation: TJclBDSInstallation;
begin
  Result := Data.Installation;
end;

function TPackageCompiler.GetPackageList: TPackageList;
begin
  Result := Data.PackageList;
end;

function TPackageCompiler.AddEnvPath(const path: string): boolean;
var
 it:string;
begin
 for it in EnvPaths do begin
  result:=LowerCase(it)=LowerCase(path);
  if result then exit;
 end;
 EnvPaths.Add(path);
 result:=SetEnvPath;
end;

procedure TPackageCompiler.AddFileToDCUPath;
  function ConvertToCurVerName(OldVerName:string):string;
  var
   suffix:string;
   VerOrd:integer;
   changed:boolean;
  begin
   verOrd:= Ord(oldvername[Length(oldvername)]);
   changed:=false;
   while (verOrd>=$30)and(verOrd<=$39) do begin
     oldvername:=StrLeft(oldVerName,Length(oldvername)-1);
     verOrd:= Ord(oldvername[Length(oldvername)]);
     changed:=true;
   end;
   suffix:=self.Installation.VersionNumberStr;
   if changed
    then result:=StrLeft(oldVerName,Length(oldvername)-1)+suffix
    else result:=oldvername;
  end;
var
  files: TStringList;
  Path,DCUPath: string;
  TagFileName,Ext,SRCFileName:string;
  PackageName:string;
  i: Integer;
  dontCopy:boolean;
begin
  assert(assigned(Data));
  files := TStringList.Create;
  files.Sorted := true;
  files.Duplicates := dupIgnore;

  try
   for i:=0 to PackageList.Count-1
    do if PackageList[i].Status=psSuccess then
    for Path in PackageList[i].SourceFilePaths
     do begin
      dontCopy:=Excluded(Path)or(Pos('~',Path)>0)or(Pos('.svn',Path)>0);
      if dontCopy then continue;
      AdvBuildFileList(PathAppend(path,'*.*'),faAnyFile,files,amAny,
           [flFullnames, flRecursive],'',nil);
     end;

   DCUPath:=PathAddSeparator(Data.DCUOutputFolder);

   if files.Count =0 then exit;

   for SRCFileName in files do begin
     ext:=LowerCase(ExtractFileExt(SRCFileName));
     dontCopy:=not FileMatch(ext)or(Pos('~',ext)>0)or(Pos('.svn',ext)>0);
     if dontCopy then continue;
     if ext='.res' then begin
       PackageName:=ConvertToCurVerName(PathExtractFileNameNoExt(SRCFileName));
       dontCopy:=PackageList.IndexOf(PackageName)>=0;
       if dontCopy then continue;
     end;
     TagFileName:=DCUPath+ExtractFileName(SRCFileName);
     FileCopy(SRCFileName,TagFileName,false);
   end;
  finally
    files.Free;
  end;
end;

procedure TPackageCompiler.Compile;
begin
 if data.Mode=coInstall
  then Installing
  else if (data.Mode=coFull)or(data.Mode=coCompile)
   then Compiling;
end;

procedure TPackageCompiler.Installing;
var
  addDCPPath,addDCUPath:boolean;
  info: TPackageInfo;
  hasInstalled:boolean;
  RTInstalled:boolean;
begin
  addDCPPath:=DirectoryExists(Data.DCPOutputFolder);
  addDCUPath:=DirectoryExists(Data.DCUOutputFolder);

  PackageList.SortList;
  hasInstalled:=false;
  RTInstalled:=false;

  for info in PackageList do
  begin
    if info.RunOnly then RTInstalled:=true;
    if InstallPackage(info,false)then hasInstalled:=true;
    if fCancel then break;
  end;

  if not hasInstalled then exit;

  installation.AddToLibrarySearchPath(Data.BPLOutputFolder,Data.Platform);
  if RTInstalled then AddEnvPath(Data.BPLOutputFolder);
  if addDCUPath then installation.AddToLibrarySearchPath(Data.DCUOutputFolder,Data.Platform);
  if addDCPPath then installation.AddToLibrarySearchPath(Data.DCPOutputFolder,Data.Platform);
end;

procedure TPackageCompiler.Compiling;
var
  i: integer;
  info: TPackageInfo;
  compileOK,CanInstall      : boolean;
  addSRCPath                : boolean;
  hasDoinstall,RTInstalled  : boolean;
begin
  if not DirectoryExists(Data.BPLOutputFolder)
   then CreateDirectory(PChar(Data.BPLOutputFolder),nil);
  if not DirectoryExists(Data.DCPOutputFolder)
   then CreateDirectory(PChar(Data.DCPOutputFolder),nil);
  if not DirectoryExists(Data.DCUOutputFolder)
   then CreateDirectory(PChar(Data.DCUOutputFolder),nil);

  if SourceFilePaths.Count = 0 then
    ResolveSourcePaths;

  addSRCPath :=Length(Data.DCUOutputFolder)=0;

  if Data.HelpFiles.Count = 0
   then ResolveHelpFiles;

  PrepareExtraOptions;

  PackageList.SortList;
  hasDoinstall:=false;
  RTInstalled:=false;
  for i := 0 to PackageList.Count - 1 do
  begin
    if fCancel then break;
    info := PackageList[i];
    compileOK:= CompilePackage(info);
    CanInstall:=(Data.Mode=coFull) and compileOK;
    if not CanInstall then continue;
    if info.RunOnly then  RTInstalled:=true;
    if InstallPackage(info,addSRCPath)then hasDoinstall:=true;
  end;

  if not hasDoinstall then exit;

  if RTInstalled then AddEnvPath(Data.BPLOutputFolder);
  installation.AddToLibrarySearchPath(Data.BPLOutputFolder,Data.Platform);

  if not AddSrcPath then AddFileToDCUPath;
  installation.AddToLibrarySearchPath(Data.DCUOutputFolder,Data.Platform);
  installation.AddToLibrarySearchPath(Data.DCPOutputFolder,Data.Platform);
end;

function TPackageCompiler.ReCompilePackage(const Info: TPackageInfo):boolean;
var
   files: TStringList;
   packDir,Path,filename: string;
begin
  result:=false;
  files := TStringList.Create;
  if not Assigned(files) then exit;
  files.Sorted := true;
  files.Duplicates := dupIgnore;
  try
   packDir:=LowerCase(ExtractFileDir(info.filename));
   files.Clear;
   for path in info.SourceFilePaths do
     AdvBuildFileList(PathAppend(path,'*.*'),faAnyFile,files
            ,amAny,[flFullnames, flRecursive],'', nil);

   for path in files do fileCopy(path,packdir,false);
   try
     case Data.Platform of
      bpWin32: Result := Installation.DCC32.MakePackage
                (Info.filename,Data.BPLOutputFolder,Data.DCPOutputFolder,fExtraOptions);
      bpWin64: Result :=installation.DCC64.MakePackage
                (Info.filename,Data.BPLOutputFolder,Data.DCPOutputFolder,fExtraOptions);
      bpOSX32: Result :=Installation.DCCOSX32.MakePackage
                (Info.filename,Data.BPLOutputFolder,Data.DCPOutputFolder,fExtraOptions);
      else result:=false;
     end;
   finally
    for path in files
      do begin
       filename:=PathAppend(packDir,ExtractFileName(path));
       if fileExists(filename)
        then if not SHDeleteFiles(0, FileName, [doSilent, doFilesOnly])
         then FileDelete(filename,true);
      end;
   end;
  finally
   files.Free;
  end;
end;

function TPackageCompiler.CompilePackage(
  const Info: TPackageInfo): Boolean;
var
   packDir:string;
   Found:boolean;
begin
  case Data.Platform of
    bpWin32: Result := Installation.DCC32.MakePackage
              (Info.filename,Data.BPLOutputFolder,Data.DCPOutputFolder,fExtraOptions);
    bpWin64: Result :=installation.DCC64.MakePackage
              (Info.filename,Data.BPLOutputFolder,Data.DCPOutputFolder,fExtraOptions);
    bpOSX32: Result :=Installation.DCCOSX32.MakePackage
              (Info.filename,Data.BPLOutputFolder,Data.DCPOutputFolder,fExtraOptions);
    else begin
      result:=false;
      exit;
    end;
  end;
  if result then exit;
  packDir:=LowerCase(ExtractFileDir(info.filename));
  found:=info.SourceFilePaths.IndexOf(packDir)>=0;
  if found then exit;
  result:=ReCompilePackage(info);
end;

procedure TPackageCompiler.PrepareExtraOptions;
var
  shortPaths,DCUPath: string;
  I: Integer;
begin
  fAllPaths.Clear;

  ExtractStrings([';'],[' '],PWideChar(Installation.LibrarySearchPath[Data.Platform]),fAllPaths);
  fAllPaths.AddStrings(SourceFilePaths);

  for I := 0 to fAllPaths.Count - 1 do
    fAllPaths[i] := Installation.SubstitutePath(fAllPaths[i]);

  shortPaths:=StrTrimQuotes(ConvertToShortPaths(fAllPaths));
  fExtraOptions := '-B -Q -CC';
  if Installation.VersionNumber>=9 then
  begin
    fExtraOptions := fExtraOptions + ' -N"SSystem.Win;Data.Win;Datasnap.Win;'
      +'Web.Win;Soap.Win;Xml.Win;Bde;Vcl;Vcl.Imaging;Vcl.Touch;Vcl.Samples;'
      +'Vcl.Shell;System;Xml;Data;Datasnap;Web;Soap;Winapi;"';
  end;
  fExtraOptions := fExtraOptions + ' -I'+shortPaths;
  fExtraOptions := fExtraOptions + ' -U'+shortPaths;
  fExtraOptions := fExtraOptions + ' -O'+shortPaths;
  fExtraOptions := fExtraOptions + ' -R'+shortPaths;

  DCUPath:=StrDoubleQuote(Data.DCUOutputFolder);
  if DCUPath<>''
   then fExtraOptions := fExtraOptions + ' -NU'+PathGetShortName(DCUPath);
  if Length(Data.Conditionals) > 0 then
    fExtraOptions := fExtraOptions + ' -D'+Data.Conditionals;
end;

function TPackageCompiler.ConvertToShortPaths(const paths : TStringList):string;
var
  path : string;
begin
  Result := '';
  for path in paths do
    Result := Result + StrDoubleQuote(PathGetShortName(path)) + ';';
end;

function TPackageCompiler.InstallPackage(const Info: TPackageInfo;addFlag:boolean): Boolean;
  function InstallDesign(BPLFileName:string):boolean;
  var
   Path:string;
  begin
   result:=FileExists(BPLFileName);
   if not result then exit;
   Result := Installation.RegisterPackage(BPLFileName, info.Description);
   if not result then exit;
   for path in info.SourceFilePaths
    do  installation.AddToLibraryBrowsingPath(path,Data.Platform);
   if addFlag
    then for path in info.SourceFilePaths
     do installation.AddToLibrarySearchPath(path,Data.Platform);
  end;
var
  BPLFileName: string;
begin
  result:=info.RunOnly;
  if result then exit;
  BPLFileName :=Info.BPLName[Data.BPLOutputFolder];
  result:=InstallDesign(BPLFileName);
end;

procedure TPackageCompiler.ResolveSourcePaths;
var
  i,j: integer;
  files, containedFiles: TStringList;
  fileExt: string;
begin
  Assert(assigned(SourceFilePaths));
  
  
  files := TStringList.Create;
  files.Sorted := true;
  files.Duplicates := dupIgnore;

  containedFiles := TStringList.Create;
  containedFiles.Sorted := true;
  containedFiles.Duplicates := dupIgnore;

  SourceFilePaths.Clear;   
  SourceFilePaths.Sorted := true;
  SourceFilePaths.Duplicates := dupIgnore;
     
  for i := 0 to PackageList.Count - 1 do
  begin
    SourceFilePaths.Add(ExtractFileDir(PackageList[i].FileName));
    for j := 0 to PackageList[i].ContainedFileList.Count - 1 do
      containedFiles.Add(ExtractFileName(PackageList[i].ContainedFileList[j]));
  end;

  AdvBuildFileList(PathAppend(fCompilationData.BaseFolder,'*.pas'),
           faAnyFile,
           files,
           amAny,
           [flFullnames, flRecursive],
           '', nil);

  AdvBuildFileList(PathAppend(fCompilationData.BaseFolder,'*.inc'),
           faAnyFile,
           files,
           amAny,
           [flFullnames, flRecursive],
           '', nil);

  for I := 0 to files.count - 1 do
  begin
    fileExt := UpperCase(ExtractFileExt(files[i]));
    if (containedFiles.IndexOf(ExtractFileName(files[i])) > 0) or (fileExt = '.INC') then
    begin
      SourceFilePaths.Add(ExtractFileDir(files[i]));
    end
  end;
end;

procedure TPackageCompiler.ResolveHelpFiles;
var
  files: TStringList;
  filename: string;
begin
  assert(assigned(Data));

  files := TStringList.Create;
  try
    AdvBuildFileList(Data.BaseFolder +'\*.hlp',
           faAnyFile,
           files,
           amAny,
           [flFullnames, flRecursive],
           '', nil);
     for filename in files do
       Data.HelpFiles.Add(filename);
  finally
    files.Free;
  end;
end;

procedure AddExclud(const FileName:string);
begin
 ExtCludFiles.Add(FileName);
end;

procedure AddInclude(const FileName:string);
begin
 IncludFiles.Add(FileName);
end;

initialization
  ExtCludFiles:=TStringList.Create;
  ExtCludFiles.Sorted := true;
  ExtCludFiles.Duplicates := dupIgnore;

  IncludFiles:=TStringList.Create;
  IncludFiles.Sorted := true;
  IncludFiles.Duplicates := dupIgnore;

  AddExclud('.txaPackage');
  AddExclud('.dpr');
  AddExclud('.bdsproj');
  AddExclud('.dpk');
  AddExclud('.bpk');
  AddExclud('.bpl');
  AddExclud('.pas');
  AddExclud('.hpp');
  AddExclud('.h');
  AddExclud('.c');
  AddExclud('.cpp');
  AddExclud('.bat');
  AddExclud('.bak');
  AddExclud('.tvsconfig');
  AddExclud('.todo');
  AddExclud('.exe');
  AddExclud('.com');
  AddExclud('.cfg');
  AddExclud('.mak');
  AddExclud('.rc');
  AddExclud('.manifest');
  AddExclud('.dproj');
  AddExclud('.identcache');
  AddExclud('.txt');
  AddExclud('.cmd');
  AddExclud('.bpg');
  AddExclud('.bdsgroup');
  AddExclud('.groupproj');
  AddExclud('.local');
  AddExclud('.rsp');
  AddExclud('.db');
  AddExclud('.map');
  AddExclud('.');
  AddExclud('..');
  AddExclud('.pdf');
  AddExclud('.bpr');
  AddExclud('.bmp');
  AddExclud('.bpi');
  AddExclud('.svn');
  AddExclud('~');

  IncludFiles.Add('.dfm');
  IncludFiles.Add('.res');
  IncludFiles.Add('.dcr');
  IncludFiles.Add('.ico');

finalization
 IncludFiles.Free;
 ExtCludFiles.Free;

end.
