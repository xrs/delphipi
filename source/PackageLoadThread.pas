{ **
  DelphiPI (Delphi Package Installer)
  Author  : ibrahim dursun (ibrahimdursun gmail)
  License : GNU General Public License 2.0
  ** }
unit PackageLoadThread;

interface

uses Classes, SysUtils, Generics.Collections, PackageInfo,
    TreeNodes, PackageInfoFactory,CompilationData;

type
  TOnThreadMsg=procedure(const msg:string)of object;
  TPackageLoadThread = class(TThread)
  private
    fPackageInfoFactory: TPackageInfoFactory;
    fActive: Boolean;
    fList: TList<TTreeNode>;
    FOnMsg: TOnThreadMsg;
    fmsg : string;
    fData:TCompilationData;
    fBaseFolder : string;
    procedure WriteMsg(const msg:string);
    procedure RaiseMsg;
    procedure LoadPackageInformations(const folder: string;var InxMajor:Integer);
    procedure LoadBPLInformations(const folder: string;var InxMajor: Integer);
    function getInstallOnly: boolean;
  protected
    procedure Execute; override;
    procedure Search(const folder: String;var InxMajor:Integer);
  public
    constructor Create(ABaseFolder:string;list: TList<TTreeNode>;AData:TCompilationData);
    destructor Destroy; override;
    property Active: Boolean read fActive write fActive;
    property OnMsg:TOnThreadMsg read FOnMsg write FOnMsg;
    property InstallOnly:boolean read getInstallOnly;
    property CompilationData:TCompilationData read fData;
    property BaseFolder:string read fBaseFolder;
  end;

implementation
uses Dialogs, JclFileUtils,gnugettext,Utils;
{ TPackageLoadThread }

constructor TPackageLoadThread.Create(ABaseFolder:string;
            list: TList<TTreeNode>;AData:TCompilationData);
begin
  inherited Create(true);
  Assert(Assigned(list), _('List cannot be null'));
  fList := list;
  fBaseFolder:=ABaseFolder;
  fData:=AData;
  fPackageInfoFactory := TPackageInfoFactory.Create;
end;

destructor TPackageLoadThread.Destroy;
begin
  fPackageInfoFactory.Free;
  inherited;
end;

procedure TPackageLoadThread.Execute;
var
 InxMajor:Integer;
begin
  inherited;
  fActive := true;
  writemsg(_('Load packages start..'));
  try
    try
      InxMajor:=0;
      Search(BaseFolder,InxMajor);
    except
      on e: Exception do
        writemsg(e.Message);
    end;
  finally
    fActive := false;
    writemsg(_('Load packages finished.'));
  end;
end;

function TPackageLoadThread.getInstallOnly: boolean;
begin
  result:=fData.Mode=coInstall;
end;

procedure TPackageLoadThread.LoadPackageInformations(const folder: string;
  var InxMajor:Integer);
var
  sr: TSearchRec;
  packageNode: TPackageTreeNode;
  packInfo:TPackageInfo;
  InxMinor:Integer;
begin
  InxMinor:=0;
  if FindFirst(PathAppend(folder, '*.DPK'), faAnyFile, sr) = 0 then
  begin
    try
      repeat
        if UpperCase(ExtractFileExt(sr.Name)) = '.DPK' then
        begin
          packInfo:=fPackageInfoFactory.CreatePackageInfo(PathAppend(folder, sr.Name));
          if packinfo=nil  then continue;

          packageNode := TPackageTreeNode.Create(packInfo);

          Inc(InxMinor);
          packageNode.InxMajor:=InxMajor;
          packageNode.InxMinor:=InxMinor;

          if not fList.Contains(packageNode) then
            fList.Add(packageNode);
        end;
      until (FindNext(sr) <> 0) and Active;
    finally
      FindClose(sr);
    end;
  end;
  if InxMinor>0 then inc(InxMajor);
end;

procedure TPackageLoadThread.LoadBPLInformations(const folder: string;
  var InxMajor:Integer);
var
  sr: TSearchRec;
  packageNode: TPackageTreeNode;
  packInfo:TPackageInfo;
  InxMinor:Integer;
begin
  InxMinor:=0;
  if FindFirst(PathAppend(folder, '*.BPL'), faAnyFile, sr) = 0 then
  begin
    try
      repeat
        if UpperCase(ExtractFileExt(sr.Name)) = '.BPL' then
        begin
          packInfo:=fPackageInfoFactory.CreateBPLInfo(PathAppend(folder, sr.Name),
            fData.Installation.VersionNumber);
          if packinfo=nil  then continue;

          packageNode := TPackageTreeNode.Create(packInfo);

          Inc(InxMinor);
          packageNode.InxMajor:=InxMajor;
          packageNode.InxMinor:=InxMinor;

          if not fList.Contains(packageNode) then
            fList.Add(packageNode);
        end;
      until (FindNext(sr) <> 0) and Active;
    finally
      FindClose(sr);
    end;
  end;
  if InxMinor>0 then inc(InxMajor);
end;

procedure TPackageLoadThread.RaiseMsg;
begin
 if Assigned(FOnMsg)
  then try FOnMsg(fmsg)except end;
end;

procedure TPackageLoadThread.Search(const folder: String;var InxMajor:Integer);
var
  directoryList: TStringList;
  directory, dir: string;
begin
  directoryList := TStringList.Create;
  Writemsg(_('Load packages in folder:')+folder);
  try
    BuildFileList(PathAppend(folder, '*.*'), faAnyFile, directoryList);
    if InstallOnly
     then LoadBPLInformations(folder,InxMajor)
     else LoadPackageInformations(folder,InxMajor);
    for directory in directoryList do
    begin
      if not Active then Break;
      dir := PathAppend(folder, directory);
      if IsDirectory(dir) then Search(dir,InxMajor);
    end;
  finally
    directoryList.Free;
  end;
end;

procedure TPackageLoadThread.WriteMsg(const msg: string);
begin
 fmsg:=msg;
 Synchronize(RaiseMsg);
end;

end.
