{**
 DelphiPI (Delphi Package Installer)
 Author  : ibrahim dursun (ibrahimdursun gmail)
 License : GNU General Public License 2.0
**}
unit PackageInfoFactory;

interface

uses SysUtils, StrUtils, Classes, PackageInfo;
type

  TPackageInfoFactory = class
  private
    procedure ReadInfo(const packageInfo: TPackageInfo; const lines: TStrings);
    function ClearStr(Str: string):string;
    procedure ResolveSourcePaths(Info : TPackageInfo);
  public
    function CreatePackageInfo(const packageFileName: string): TPackageInfo; overload;
    function CreatePackageInfo(const packageFileContent: TStrings): TPackageInfo; overload;
    function CreateBPLInfo(const BPLFileName: string;ComplierVersion:Integer): TPackageInfo;
  end;

implementation

uses JclFileUtils,jclPEImage;

const
  StrRUNONLY = '{$RUNONLY';
  StrDESIGNONLY = '{$DESIGNONLY';//2014.7.19 Rishunxue
  StrDESCRIPTION = '{$DESCRIPTION ';
  StrSUFFIX = '{$LIBSUFFIX ';
  StrPackage = 'package';
  StrRequires = 'requires';
  StrContains = 'contains';

{ TPackageInfoFactory }

function TPackageInfoFactory.ClearStr(Str: string): string;
begin
  Result := Trim(Str);
  Result := ReplaceStr(Result, ',', '');
  Result := LowerCase(ReplaceStr(Result, ';', ''));
end;

function TPackageInfoFactory.CreatePackageInfo(
  const packageFileName: string): TPackageInfo;
var
  lines: TStrings;
  NameFromFileName:string;
  NameFromFile:string;
begin
  lines := TStringList.Create;
  try
    lines.LoadFromFile(packageFileName);
    result := CreatePackageInfo(lines);
    Result.FileName := packageFileName;
    NameFromFile:=LowerCase(result.PackageName);
    NameFromFileName:=LowerCase(PathExtractFileNameNoExt(packageFileName));
    if NameFromFile=NameFromFileName
     then ResolveSourcePaths(result)
     else FreeAndNil(result);
  finally
    lines.Free;
  end;
end;

function TPackageInfoFactory.CreateBPLInfo(const BPLFileName: string;
  ComplierVersion:Integer): TPackageInfo;
var
 bpl:TJclPeBorImage;
 reqPack:string;
begin
 bpl:=TJclPeBorImage.Create(true);
 result:=nil;
 try
  bpl.FileName:=BPLFileName;
  if not bpl.StatusOK or not bpl.IsPackage
    then exit;
  Result := TPackageInfo.Create();
  result.Description:=bpl.PackageInfo.Description;
  result.PackageName:=PathExtractFileNameNoExt(bpl.FileName);
  result.DesignOnly:=((pfRunOnly and bpl.PackageInfo.Flags)<>pfRunOnly)
    or ((pfDesignOnly and bpl.PackageInfo.Flags)=pfDesignOnly) ;
  result.FileName:=bpl.FileName;
  result.RequiredPackageList.Clear;
  for reqPack in bpl.PackageInfo.Requires
   do result.RequiredPackageList.Add(PathExtractFileNameNoExt(reqPack));
  result.ContainedFileList.AddStrings(bpl.PackageInfo.Contains);
 finally
  bpl.Free;
 end;
end;

function TPackageInfoFactory.CreatePackageInfo(
  const packageFileContent: TStrings): TPackageInfo;
begin
  assert(packageFileContent <> nil);
  Result := TPackageInfo.Create();
  ReadInfo(Result, packageFileContent);
end;

// ugly code, but implementing a full blown parser here is like nailing with a sledge hammer
procedure TPackageInfoFactory.ReadInfo(const packageInfo: TPackageInfo; const lines: TStrings);
var
  I,inx: Integer;
  Str,tmp : String;
  bDesignOnly,bRunOnly,RequiresBlock,ContainsBlock: Boolean;
begin
  assert(lines <> nil);
  assert(packageInfo <> nil);
  
  RequiresBlock := False;
  ContainsBlock := False;
  bRunOnly:=false;
  bDesignOnly :=false;
  for I := 0 to lines.Count - 1 do
  begin
    Str := Trim(lines[i]);
    if not bDesignOnly and not bRunOnly
     then begin
      if Pos(StrDESIGNONLY+'}',str) = 1 then
       bDesignOnly := True; //2014.7.19 Rishunxue

      if (Pos(StrDESIGNONLY,str) = 1) and (Pos('ON',Str)>0) then
       bDesignOnly:= True;//2014.7.19 Rishunxue

      if not bDesignOnly then
       begin
        if Pos(StrRUNONLY+'}',str) = 1 then
         bRunOnly := True;

        if (Pos(StrRUNONLY,str) = 1) and (Pos('ON',Str)>0) then
         bRunOnly:= True;
       end;
     end;

    if Pos(StrDESCRIPTION,str) > 0 then
      packageInfo.Description := Copy(Str,Length(StrDescription)+2,Pos('}',str)-Length(StrDescription)-3);
    if Pos(StrSUFFIX,str) > 0 then
      packageInfo.AddSuffix(Copy(Str,Length(StrSUFFIX)+2,Pos('}',str)-Length(StrSUFFIX)-3));

    if Pos(StrPackage,str) = 1 then
      packageInfo.PackageName := Trim(Copy(Str,Length(strPackage)+2,Length(Str)- Length(strPackage) -2));

    if Pos(StrRequires,str) = 1 then RequiresBlock := True;
    if RequiresBlock then
    begin
      if not StartsStr('{$', Str) then
        packageInfo.RequiredPackageList.Add(ClearStr(Str));
    end;
    if (RequiresBlock) and (pos(';',str) > 0) then
      RequiresBlock := False;

    if Pos(StrContains,str) = 1 then ContainsBlock := True;

    if ContainsBlock then begin
      inx:=Pos(' in ',Str);
      if inx > 0
       then  tmp:=Copy(str,1,inx-1)
       else  tmp:=str;
      if not StartsStr('{$', Str) then
         packageInfo.ContainedFileList.Add(ClearStr(tmp)+ '.pas');
    end;
    if (ContainsBlock) and (pos(';',str) > 0) then
      ContainsBlock := False;
  end;

  with packageInfo do if RequiredPackageList.Count > 0 then
  begin
    RequiredPackageList.Delete(0);
    if not bRunOnly and not bDesignOnly
     then begin
      bDesignOnly:=RequiredPackageList.IndexOf('designide')>=0;
     end;
  end;
  packageInfo.DesignOnly:=bDesignOnly;
  if packageInfo.ContainedFileList.Count > 0 then
    packageInfo.ContainedFileList.Delete(0);
end;

procedure TPackageInfoFactory.ResolveSourcePaths(Info : TPackageInfo);
const
 cpack='package';
var
  i,inx: integer;
  files: TStringList;
  fileExt,PackPath,SourcePath,SourcesPath: string;
begin
  Assert(assigned(Info.SourceFilePaths));


  files := TStringList.Create;
  files.Sorted := true;
  files.Duplicates := dupIgnore;

  info.SourceFilePaths.Clear;
  info.SourceFilePaths.Sorted := true;
  info.SourceFilePaths.Duplicates := dupIgnore;
 try
  PackPath:=lowercase(ExtractFileDir(Info.FileName));
  inx:=0;
  repeat
   i:=PosEx(cpack,packPath,inx+1);
   if i>0 then inx:=i+length(cpack)-1;
  until i=0;
  inx:=inx-length(cpack);
  SourcesPath:='';
  SourcePath:='';
  if (inx>0)
   then begin
    SourcesPath:=copy(packPath,1,inx)+'sources';
    SourcePath:=copy(packPath,1,inx)+'source';
   end;

  AdvBuildFileList(PathAppend(PackPath,'*.pas'),faAnyFile,files
            ,amAny,[flFullnames, flRecursive],'', nil);

  AdvBuildFileList(PathAppend(PackPath,'*.inc'),faAnyFile,files
            ,amAny,[flFullnames, flRecursive],'', nil);

  if SourcesPath<>'' then begin
   AdvBuildFileList(PathAppend(SourcesPath,'*.pas'),faAnyFile,files
            ,amAny,[flFullnames, flRecursive],'', nil);

   AdvBuildFileList(PathAppend(SourcesPath,'*.inc'),faAnyFile,files
            ,amAny,[flFullnames, flRecursive],'', nil);
  end;

  if SourcePath<>'' then begin
   AdvBuildFileList(PathAppend(SourcePath,'*.pas'),faAnyFile,files
            ,amAny,[flFullnames, flRecursive],'', nil);

   AdvBuildFileList(PathAppend(SourcePath,'*.inc'),faAnyFile,files
            ,amAny,[flFullnames, flRecursive],'', nil);
  end;

  for I := 0 to files.count - 1 do
  begin
    fileExt := UpperCase(ExtractFileExt(files[i]));
    if (info.ContainedFileList.IndexOf(ExtractFileName(files[i])) > 0) or (fileExt = '.INC') then
    begin
      info.SourceFilePaths.Add(ExtractFileDir(files[i]));
    end
  end;
 finally
   files.Free;
 end;
end;

end.
