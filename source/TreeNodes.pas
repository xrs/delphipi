{ **
  DelphiPI (Delphi Package Installer)
  Author  : ibrahim dursun (ibrahimdursun gmail)
  License : GNU General Public License 2.0
** }
unit TreeNodes;
interface
uses treemodel, packageinfo, Generics.Defaults;
type
  TNodeType = (ntNode, ntFolder, ntPackage);
  TTreeNode = class(TInterfacedObject, INode)
  private
    fName: string;
    fPath: string;
    fSelected: boolean;
    fNodeType: TNodeType;
  protected
    function getInxMajor: Integer;virtual;
    function getInxMinor: Integer;virtual;
    procedure setInxMajor(const Value: Integer);virtual;
    procedure setInxMinor(const Value: Integer);virtual;
  public
    constructor Create(const name, path: string);
    function GetData: TObject; virtual;
    function GetDisplayName: string; virtual;
    function GetNodePath: string; virtual;
    property NodeType: TNodeType read fNodeType;
    property Selected: boolean read fSelected write fSelected;
    property InxMajor: Integer read getInxMajor write setInxMajor;
    property InxMinor: Integer read getInxMinor write setInxMinor;
  end;

  TPackageTreeNode = class(TTreeNode)
  private
    fInfo: TPackageInfo;
    fMissingPackageName: string;
  protected
    function getInxMajor: Integer; override;
    function getInxMinor: Integer; override;
    procedure setInxMajor(const Value: Integer); override;
    procedure setInxMinor(const Value: Integer); override;
  public
    constructor Create(const info: TPackageInfo); virtual;
    function GetData: TObject; override;
    function GetDisplayName: string; override;
    function GetNodePath: string; override;
    function ToString: string; override;
    property MissingPackageName: string read fMissingPackageName write fMissingPackageName;
    property Info:TPackageInfo read fInfo;
  end;

  TTreeNodeComparer = class(TInterfacedObject, IComparer<TTreeNode>)
    function Compare(const Left, Right: TTreeNode): Integer;
  end;


implementation
uses JclStrings;

constructor TTreeNode.Create(const name, path: string);
begin
  fName := name;
  fPath := path;
  fNodeType := ntFolder;
  fSelected := true;
end;

function TTreeNode.GetData: TObject;
begin
  Result := nil;
end;

function TTreeNode.GetDisplayName: string;
begin
  Result := fName;
end;

function TTreeNode.getInxMajor: Integer;
begin
 result:=0;
end;

function TTreeNode.getInxMinor: Integer;
begin
 result:=0;
end;

function TTreeNode.GetNodePath: string;
begin
  Result := fPath;
end;

procedure TTreeNode.setInxMajor(const Value: Integer);
begin
end;

procedure TTreeNode.setInxMinor(const Value: Integer);
begin
end;

constructor TPackageTreeNode.Create(const info: TPackageInfo);
begin
  fInfo := info;
  fNodeType := ntPackage;
  fSelected := true;
end;

function TPackageTreeNode.GetData: TObject;
begin
  Result := fInfo;
end;

function TPackageTreeNode.GetDisplayName: string;
begin
  Result := fInfo.PackageName;
end;

function TPackageTreeNode.getInxMajor: Integer;
begin
 assert(assigned(self.fInfo));
 result:=fInfo.InxMajor;
end;

function TPackageTreeNode.getInxMinor: Integer;
begin
 assert(assigned(self.fInfo));
 result:=fInfo.InxMinor;
end;

function TPackageTreeNode.GetNodePath: string;
var
  i: integer;
begin
  Result := fInfo.FileName;
  i := Pos(':', Result);
  if i <> 0 then
    Result := StrRestOf(Result, i + 2);
end;

procedure TPackageTreeNode.setInxMajor(const Value: Integer);
begin
 assert(assigned(self.fInfo));
 fInfo.InxMajor:=value;
end;

procedure TPackageTreeNode.setInxMinor(const Value: Integer);
begin
 assert(assigned(self.fInfo));
 fInfo.InxMinor:=value;
end;

function TPackageTreeNode.ToString: string;
begin
  Result := fInfo.FileName;
end;

{ TTreeNodeComparer }

function TTreeNodeComparer.Compare(const Left, Right: TTreeNode): Integer;
begin
  Result := StrCompare(Left.GetNodePath, Right.GetNodePath, true);
end;

end.
