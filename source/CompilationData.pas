{**
 DelphiPI (Delphi Package Installer)
 Author  : ibrahim dursun (ibrahimdursun gmail)
 License : GNU General Public License 2.0
**}
unit CompilationData;
interface
uses Classes, PackageInfo, PackageList,JclIDEUtils;

type
  TCompilationData = class
  private
    fBaseFolder: String;
    fInstallation: TJclBDSInstallation;
    fPackageList: TPackageList;
    fHelpFiles: TStringList;
    fPattern: String;
    fDCPOutputFolder: string;
    fBPLOutputFolder: string;
    fDCUOutputFolder: string;
    FScripting: Boolean;
    fConditionals: string;
    fMode : Integer;//rishunxue 2010.3.26
    //Compile mode 0-Compile and Install 1-compile only
    fPlatform:TJclBDSPlatform;//rishunxue 2014.07.19 Target Platform
    procedure SetPackageList(const aPackageList: TPackageList);
    procedure SetInstallation(const Value: TJclBDSInstallation);
    procedure SetPlatform(const Value: TJclBDSPlatform);
    function GetPlatform: TJclBDSPlatform;
  protected
  public
    constructor Create;
    destructor Destroy; override;

    procedure GetIdePackages(const list: TStringList); virtual;  
    function GetIdeVersionSuffix: string; virtual;
    function SetDelphiVersion(const version:string):boolean; virtual;
    
    property Pattern: String read fPattern write fPattern;
    property Installation: TJclBDSInstallation read fInstallation write SetInstallation;
    property BaseFolder: String read fBaseFolder write fBaseFolder;
    property HelpFiles: TStringList read fHelpFiles;
    property PackageList: TPackageList read fPackageList write SetPackageList;
    property DCPOutputFolder: string read fDCPOutputFolder write fDCPOutputFolder;
    property BPLOutputFolder: string read fBPLOutputFolder write fBPLOutputFolder;
    property DCUOutputFolder: string read fDCUOutputFolder write fDCUOutputFolder;
    property Conditionals: string read fConditionals write fConditionals;

    property Scripting: Boolean read fScripting write fScripting;
    property Platform:TJclBDSPlatform read GetPlatform write SetPlatform;
    property Mode: Integer read fMode write fMode;//rishunxue 2010.3.26
  end;

implementation

uses JclFileUtils,SysUtils,gnugettext;
var
  installations: TJclBorRADToolInstallations;

constructor TCompilationData.Create;
begin
  fPattern := '*.dpk';
  fPackageList := TPackageList.Create;
  fHelpFiles := TStringList.Create;
  fScripting := False;
  fPlatform:=bpWin32;
end;

destructor TCompilationData.Destroy;
begin
  fPackageList.Free;
  fHelpFiles.Free;
  inherited;
end;

procedure TCompilationData.GetIdePackages(const list: TStringList);
var
 i: integer;
begin
  assert(Assigned(Installation),_('installation cannot be null'));
 
  for i := 0 to Installation.IdePackages.Count - 1 do
    list.Add(Installation.IdePackages.PackageFileNames[i]);
end;

function TCompilationData.GetIdeVersionSuffix: string;
begin
  Result := Installation.VersionNumberStr;
  if Result = 'd11' then //Delphi 2007 packages have version 10 extension
    Result := 'd10';
end;

function TCompilationData.GetPlatform: TJclBDSPlatform;
begin
  if fInstallation.VersionNumber<10
   then result:=bpWin32
   else result:=fPlatform;
end;

function TCompilationData.SetDelphiVersion(const version: string): boolean;
var
  installation: TJclBDSInstallation;
  i : integer;
begin
  Result := false;
  for i := 0 to installations.Count - 1 do
  begin
    installation := installations.Installations[i] as TJclBDSInstallation;
    if UpperCase(Trim(installation.VersionNumberStr)) = UpperCase(Trim(version)) then
    begin
      fInstallation := installation;
      Result := true;
      break;
    end;
  end;
  if fInstallation = nil then
    raise Exception.Create(_('cannot find delphi version:') + version);
end;

procedure TCompilationData.SetInstallation(
  const Value: TJclBDSInstallation);
var
 old: TJclBorRADToolInstallation;
begin
  if fInstallation = Value then
    exit;
  old:=fInstallation;

  fInstallation := Value;
  if old<>nil
   then if old.VersionNumber=value.VersionNumber  then exit;

  BPLOutputFolder := fInstallation.BPLOutputPath[Platform];
  DCPOutputFolder := fInstallation.DCPOutputPath[Platform];
end;

procedure TCompilationData.SetPackageList(const aPackageList: TPackageList);
begin
  fPackageList.Free;
  fPackageList := aPackageList;
end;

procedure TCompilationData.SetPlatform(const Value: TJclBDSPlatform);
begin
  if fInstallation.VersionNumber < 10
   then fPlatform:=bpWin32
   else fPlatform := Value;
end;

initialization
  installations := TJclBorRADToolInstallations.Create;
finalization
  installations.Free;  

end.
