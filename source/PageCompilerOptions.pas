{**
 DelphiPI (Delphi Package Installer)
 Author  : ibrahim dursun (ibrahimdursun gmail)
 License : GNU General Public License 2.0
**}
unit PageCompilerOptions;

interface

uses
  Classes, CompilationData, Windows, Messages, SysUtils, Variants, Graphics, Controls, Forms,
  Dialogs, PageBase, StdCtrls, ExtCtrls, WizardIntfs;

type
  TSelectCompilerOptions = class(TWizardPage)
    grpOutputFolders: TGroupBox;
    lblBPLOutputFolder: TLabel;
    edtBPL: TEdit;
    btnBPLBrowse: TButton;
    lblDCP: TLabel;
    edtDCP: TEdit;
    btnDCPBrowse: TButton;
    lblDCU: TLabel;
    edtDCU: TEdit;
    btnDCUBrowse: TButton;
    Label1: TLabel;
    edtConditionals: TEdit;
    rgMode: TRadioGroup;
    rgPlatForm: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBPLBrowseClick(Sender: TObject);
    procedure btnDCPBrowseClick(Sender: TObject);
    procedure btnDCUBrowseClick(Sender: TObject);
    procedure edtBPLExit(Sender: TObject);
    procedure edtBPLEnter(Sender: TObject);
    procedure edtBPLDblClick(Sender: TObject);
  private
    fOldBPL:string;
    procedure SaveInstallationOutputFolders;
    procedure ShowInstallationOutputFolders;
    procedure BrowseFolder(var folder: string);
    function SelectFolder(const editBox: TEdit): string;
    procedure NodesClear;
  public
    constructor Create(Owner: TComponent; const compilationData: TCompilationData); override;
    procedure UpdateWizardState; override;
    function CanShowPage: Boolean; override;
  end;

var
  SelectCompilerOptions: TSelectCompilerOptions;

implementation

uses gnugettext, FileCtrl,JclSysInfo,Generics.Collections, TreeNodes,JclIDEUtils;
{$R *.dfm}

{ TSelectDelphiInstallationPage }

constructor TSelectCompilerOptions.Create(Owner: TComponent;
  const compilationData: TCompilationData);
begin
  inherited;
  TranslateComponent(self);
end;

procedure TSelectCompilerOptions.edtBPLDblClick(Sender: TObject);
var
 edt:TEdit absolute Sender;
begin
 if Assigned(sender)
  then edt.SelectAll;
end;

procedure TSelectCompilerOptions.edtBPLEnter(Sender: TObject);
begin
 fOldBPL:=edtBPL.Text;
end;

procedure TSelectCompilerOptions.edtBPLExit(Sender: TObject);
begin
  if fOldBPL=edtBPL.Text  then exit;
  edtDCP.Text:=StringReplace(edtBPL.Text,'bpl','DCP',[rfReplaceAll, rfIgnoreCase]);
  edtDCU.Text:=StringReplace(edtBPL.Text,'bpl','DCU',[rfReplaceAll, rfIgnoreCase]);
end;

procedure TSelectCompilerOptions.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  SaveInstallationOutputFolders;
end;

procedure TSelectCompilerOptions.FormCreate(Sender: TObject);
begin
  inherited;
  TranslateComponent(self);
  ShowInstallationOutputFolders;
end;

procedure TSelectCompilerOptions.BrowseFolder(var folder:string);
var
  directory: string;
begin
  directory := '';
  if SelectDirectory(_('Select output folder'),'',directory) then
  begin
     folder := directory;
  end;
end;

function TSelectCompilerOptions.SelectFolder(const editBox: TEdit):string;
var
  path: string;
begin
  inherited;
  Assert(Assigned(editBox));
  path := editBox.Text;
  BrowseFolder(path);
  editBox.Text := path;
end;

procedure TSelectCompilerOptions.btnBPLBrowseClick(Sender: TObject);
begin
  inherited;
  SelectFolder(edtBPL);
  edtDCP.Text:=edtBPL.Text;
  edtDCU.Text:=edtBPL.Text;
end;

procedure TSelectCompilerOptions.btnDCPBrowseClick(Sender: TObject);
begin
  inherited;
  SelectFolder(edtDCP);
end;

procedure TSelectCompilerOptions.btnDCUBrowseClick(Sender: TObject);
begin
  inherited;
  SelectFolder(edtDCU);
end;

function TSelectCompilerOptions.CanShowPage: Boolean;
begin
   Result := True;
end;

procedure TSelectCompilerOptions.NodesClear;
var
 node:TTreeNode;
 nodes:TList<TTreeNode>;
begin
  nodes := TList<TTreeNode>(Wizard.GetState('nodes'));
  if Assigned(nodes)
   then begin
    for node in nodes do node.Free;
    nodes.Clear;
   end;
end;

procedure TSelectCompilerOptions.SaveInstallationOutputFolders;
begin
  Assert(Assigned(Data), 'Compilation data is null');
  Data.BPLOutputFolder := trim(lowerCase(edtBPL.Text));
  Data.DCPOutputFolder := trim(lowerCase(edtDCP.Text));
  Data.DCUOutputFolder := trim(lowerCase(edtDCU.Text));
  Data.Conditionals := edtConditionals.Text;
  case rgPlatForm.ItemIndex of
   1:Data.Platform:=bpWIN64;
   2:Data.Platform:=bpOSX32;
   else Data.Platform:=bpWIN32;
  end;
  if Data.Mode <> rgMode.ItemIndex
   then begin
    NodesClear;
    Data.Mode:=rgMode.ItemIndex;
   end;
end;

procedure TSelectCompilerOptions.ShowInstallationOutputFolders;
begin
  Assert(Assigned(Data), 'Compilation data is null');
  edtBPL.OnExit:=nil;
  edtBPL.OnEnter:=nil;
  edtBPL.Text:= Data.BPLOutputFolder;
  edtDCP.Text:= Data.DCPOutputFolder;
  edtDCU.Text:= Data.DCUOutputFolder;
  edtConditionals.Text := Data.Conditionals;
  rgMode.ItemIndex:=Data.Mode;
  case Data.Platform of
   bpWIN64:rgPlatForm.ItemIndex:=1;
   bpOSX32:rgPlatForm.ItemIndex:=2;
   else rgPlatForm.ItemIndex:=0;
  end;
  rgPlatForm.Visible:=Data.Installation.PackageVersionNumber>=16;
  edtBPL.OnExit:=edtBPLExit;
  edtBPL.OnEnter:=edtBPLEnter;
end;

procedure TSelectCompilerOptions.UpdateWizardState;
begin
  inherited;
  wizard.SetHeader(_('Select Output Folders and Compiler Conditionals'));
  wizard.SetDescription(_('Please select output folders and compiler conditionals that will affect the compilation'));
end;

end.
