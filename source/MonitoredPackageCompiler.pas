{**
 DelphiPI (Delphi Package Installer)
 Author  : ibrahim dursun (ibrahimdursun gmail)
 License : GNU General Public License 2.0
**}
unit MonitoredPackageCompiler;

interface

uses progressmonitor, packagecompiler, packageinfo, CompilationData;

type
  TPackageCompileEvent = procedure(const package: TPackageInfo; status: TPackageStatus) of object;
  TMonitoredPackageCompiler = class(TPackageCompiler)
  private
    fMonitor: IProgressMonitor;
    procedure ShowPackageInfo(const Info: TPackageInfo);
  protected
    procedure RaiseEvent(const packageInfo: TPackageInfo; status: TPackageStatus);virtual;
    procedure PrepareExtraOptions; override;
    function CompilePackage(const Info: TPackageInfo): boolean; override;
    function InstallPackage(const Info: TPackageInfo;addFlag:boolean): boolean; override;
    function ReCompilePackage(const Info: TPackageInfo): boolean;override;
  public
    constructor Create(const compilationData: TCompilationData; const aMonitor: IProgressMonitor); reintroduce;
    procedure ResolveSourcePaths; override;
    procedure CompilerOutputCallback(const line:string); virtual;
    procedure Compile; override;
  end;

implementation

uses Classes,JclFileUtils,gnugettext,JclPeImage,SysUtils,Utils,JclIDEUtils;

constructor TMonitoredPackageCompiler.Create(const compilationData:TCompilationData; const aMonitor: IProgressMonitor);
begin
  Assert(aMonitor <> nil,_('monitor cannot be null')); { TODO -oidursun -c : a default null implementation can be set }
  inherited Create(compilationData);
  fMonitor := aMonitor;
end;

procedure TMonitoredPackageCompiler.Compile;
begin
  fMonitor.Started;
  fMonitor.Log(_('-=Started'));
  inherited;
  fMonitor.Finished;
  fMonitor.Log(_('-=Finished'));
end;

procedure TMonitoredPackageCompiler.ShowPackageInfo(const Info: TPackageInfo);
var
  img:TJclPeBorImage;
  BPLFileName,filename:string;
begin
  BPLFileName:=Info.BPLName[Data.BPLOutputFolder];
  if not FileExists(BPLFileName) then exit;
  img:=TJclPeBorImage.Create;
  try
   img.FileName:=BPLFileName;
   if not img.IsPackage then exit;
   with img.PackageInfo do begin
    fMonitor.Log('Description:'+Description);
    fMonitor.Log('DCPName:'+DcpName);
    fMonitor.Log('Contains:');
    for filename in Contains  do fMonitor.Log('  '+fileName);
    fMonitor.Log('Requires:');
    for filename in Requires do fMonitor.Log('  '+fileName);
   end;
  finally
   img.Free;
  end;
end;

function TMonitoredPackageCompiler.CompilePackage(
  const Info: TPackageInfo): boolean;
begin
  fMonitor.Log(_('-=Compiling: ') + Info.PackageName);
  fMonitor.Log(_('Required :')+Info.RequiredPackageList.DelimitedText);
  fMonitor.Log(_('Contains :')+Info.ContainedFileList.DelimitedText);

  RaiseEvent(Info, psCompiling);

  if Info.DesignOnly and (Data.Platform<>bpWin32) //Rishunxue 2014.7.19
  then begin;
   fMonitor.CompilerOutput('Error:'+_('Unsupport to compile Win64 Design Package'));
   fMonitor.Log(_('Finished'));
   result:=false;
   exit;
  end;

  case Data.Platform of
    bpWin32: Installation.DCC32.OutputCallback := CompilerOutputCallback;
    bpWin64: Installation.DCC64.OutputCallback := CompilerOutputCallback;
    bpOSX32: Installation.DCCOSX32.OutputCallback := CompilerOutputCallback;
  end;

  Result := inherited;
  if not Result then begin
    RaiseEvent(Info, psError);
    fMonitor.Log(_('compile failed'));
  end
  else begin
    RaiseEvent(Info, psSuccess);
    fMonitor.Log(_('compile succedded'));
    ShowPackageInfo(info);
  end;
  fMonitor.Log(_('Finished'));
end;

function TMonitoredPackageCompiler.ReCompilePackage(
  const Info: TPackageInfo): boolean;
begin
  fMonitor.Log(_('-=ReCompiling: ') + Info.PackageName);
  RaiseEvent(Info, psReCompiling);
  Result := inherited;
  if result  then ShowPackageInfo(info);
end;

procedure TMonitoredPackageCompiler.CompilerOutputCallback(const line: string);
begin
  fMonitor.Log('      '+line);
  fMonitor.CompilerOutput(line);
end;

function TMonitoredPackageCompiler.InstallPackage(
  const Info: TPackageInfo;addFlag:boolean): boolean;
var
  BPLFileName : String;
begin
  if Data.Mode=coInstall
   then BPLFileName := info.FileName
   else BPLFileName := Info.BPLName[Data.BPLOutputFolder];
  fMonitor.Log(_('-=Installing: ') + BPLFileName);
  RaiseEvent(Info, psInstalling);
  Result := inherited ;
  if not Result then begin
    RaiseEvent(Info, psError);
    fMonitor.Log(_('install failed'));
  end
  else begin
    RaiseEvent(Info, psSuccess);
    fMonitor.Log(_('install succedded'));
  end;
  fMonitor.Log(_('Finished'));
end;

procedure TMonitoredPackageCompiler.PrepareExtraOptions;
var
  line: string;
begin
  inherited;
  fMonitor.Log(_('-=All Source Path:'));
  for line in AllPaths do
    fMonitor.Log(line);
  fMonitor.Log(_('-=Compiler Options:'));
  fMonitor.Log(ExtraOptions);
end;

procedure TMonitoredPackageCompiler.RaiseEvent(const packageInfo: TPackageInfo;
  status: TPackageStatus);
begin
  fMonitor.PackageProcessed(packageInfo, status);
  if (status = psSuccess) or (status = psError) then
    packageInfo.Status := status;
end;

procedure TMonitoredPackageCompiler.ResolveSourcePaths;
var
  path: string;
begin
  inherited;
  fMonitor.Log(_('-=Source Paths:'));
  for path in SourceFilePaths do
    fMonitor.Log(path);
end;

end.
