{**
 DelphiPI (Delphi Package Installer)
 Author  : ibrahim dursun (ibrahimdursun gmail)
 License : GNU General Public License 2.0
**}
unit Utils;

interface
uses classes,Windows,JclIDEUtils; 
const
  VERSION = '0.71(+)';
  CODE = 'Marauder(Modify By Rishunxue)';
  AUTHOR = 'Inrahim DURSUN(Modify By Rishunxue)';

  //delphi compiler versions
  DELPHI_VERSION_UNKNOWN = -1;
  DELPHI_VERSION_MIN = 0;
  DELPHI_VERSION_5 = DELPHI_VERSION_MIN;
  DELPHI_VERSION_6 = DELPHI_VERSION_MIN+1;
  DELPHI_VERSION_7 = DELPHI_VERSION_MIN+2;
  DELPHI_VERSION_8 = DELPHI_VERSION_MIN+3;
  DELPHI_VERSION_2005 = DELPHI_VERSION_MIN+4;
  DELPHI_VERSION_2006 = DELPHI_VERSION_MIN+5;
  DELPHI_VERSION_2007 = DELPHI_VERSION_MIN+6;
  DELPHI_VERSION_2009 = DELPHI_VERSION_MIN+7;
  DELPHI_VERSION_2010 = DELPHI_VERSION_MIN+8;
  DELPHI_VERSION_XE1 = DELPHI_VERSION_MIN+9;
  DELPHI_VERSION_XE2 = DELPHI_VERSION_MIN+10;
  DELPHI_VERSION_XE3 = DELPHI_VERSION_MIN+11;
  DELPHI_VERSION_XE4 = DELPHI_VERSION_MIN+12;
  DELPHI_VERSION_XE5 = DELPHI_VERSION_MIN+13;
  DELPHI_VERSION_XE6 = DELPHI_VERSION_MIN+14;

  DELPHI_VERSION_MAX = DELPHI_VERSION_XE6;
  VersionNames: array[DELPHI_VERSION_UNKNOWN..DELPHI_VERSION_MAX] of string
   = ('Unknown', 'Delphi 5','Delphi 6','Delphi 7','Delphi 8','Delphi 2005',
    'Delphi 2006', 'Delphi 2007','Delphi 2009','Delphi 2010','Delphi XE',
    'Delphi XE2','Delphi XE3','Delphi XE4','Delphi XE5','Delphi XE6'
    );

  coFull=0;
  coCompile=1;
  coInstall=2;

type
  TDelphiVersionArray = array[DELPHI_VERSION_MIN..DELPHI_VERSION_MAX] of TStrings;
function GuessDelphiVersion(name: string): integer;
function GuessDelphiVerStr(name: string): string;

implementation

uses JclFileUtils, SysUtils,StrUtils;
var
  i:Integer;
  patterns: TDelphiVersionArray;

procedure AddDelphiPattern(const version: integer; const pattern: string);
var
 rPattern:string;
begin
 rPattern:=ReverseString(pattern);
 rPattern:=UpperCase(rPattern);
 patterns[version].Add(rPattern);
end;

function GuessDelphiVerStr(name: string): string;
var
 ver:Integer;
begin
 ver:=GuessDelphiVersion(name);
 case ver of
  DELPHI_VERSION_5 : result:='d5';
  DELPHI_VERSION_6 : result:='d6';
  DELPHI_VERSION_7 : result:='d7';
  DELPHI_VERSION_8 : result:='d8';
  DELPHI_VERSION_2005 : result:='d9';
  DELPHI_VERSION_2006 : result:='d10';
  DELPHI_VERSION_2007 : result:='d11';
  DELPHI_VERSION_2009 : result:='d13';
  DELPHI_VERSION_2010 : result:='d14';
  DELPHI_VERSION_XE1  : result:='d15';
  DELPHI_VERSION_XE2  : result:='d16';
  DELPHI_VERSION_XE3  : result:='d17';
  DELPHI_VERSION_XE4  : result:='d18';
  DELPHI_VERSION_XE5  : result:='d19';
  DELPHI_VERSION_XE6  : result:='d20';
  else result:='';
 end;
end;

function GuessDelphiVersion(name: string): integer;
var
  pattern : string;
  found : boolean;
  rName:string;
begin
  name := JclFileUtils.PathExtractFileNameNoExt(name);
  rName:=ReverseString(name);
  rName:=UpperCase(rName);
  result:=DELPHI_VERSION_UNKNOWN;
  for i :=DELPHI_VERSION_MAX downto DELPHI_VERSION_MIN do
  begin
    found:=false;
    for pattern in  patterns[i] do
    begin
       found := Pos(pattern,rName)=1;
       if found then begin
        result:=i;
        break;
       end;
    end;
    if found then break;
  end;
end;

initialization
  // delphi compiler versions: http://docwiki.embarcadero.com/RADStudio/XE5/en/Compiler_Versions
  for I := DELPHI_VERSION_MIN to DELPHI_VERSION_MAX do
    patterns[I] := TStringList.Create;

  AddDelphiPattern(DELPHI_VERSION_5, 'delphi5');
  AddDelphiPattern(DELPHI_VERSION_5, 'd50');
  AddDelphiPattern(DELPHI_VERSION_5, '50');
  AddDelphiPattern(DELPHI_VERSION_5, '_5');
  AddDelphiPattern(DELPHI_VERSION_5, 'r5');
  AddDelphiPattern(DELPHI_VERSION_5, 'd5');
  AddDelphiPattern(DELPHI_VERSION_5, '5');

  AddDelphiPattern(DELPHI_VERSION_6, 'delphi6');
  AddDelphiPattern(DELPHI_VERSION_6, 'd60');
  AddDelphiPattern(DELPHI_VERSION_6, 'r60');
  AddDelphiPattern(DELPHI_VERSION_6, '60');
  AddDelphiPattern(DELPHI_VERSION_6, '_6');
  AddDelphiPattern(DELPHI_VERSION_6, 'r6');
  AddDelphiPattern(DELPHI_VERSION_6, 'd6');
  AddDelphiPattern(DELPHI_VERSION_6, '6');

  AddDelphiPattern(DELPHI_VERSION_7, 'delphi7');
  AddDelphiPattern(DELPHI_VERSION_7, 'd70');
  AddDelphiPattern(DELPHI_VERSION_7, 'r70');
  AddDelphiPattern(DELPHI_VERSION_7, '_70');
  AddDelphiPattern(DELPHI_VERSION_7, '70');
  AddDelphiPattern(DELPHI_VERSION_7, '_7');
  AddDelphiPattern(DELPHI_VERSION_7, 'r7');
  AddDelphiPattern(DELPHI_VERSION_7, 'd7');
  AddDelphiPattern(DELPHI_VERSION_7, '7');

  AddDelphiPattern(DELPHI_VERSION_8, 'D80');
  AddDelphiPattern(DELPHI_VERSION_8, 'D8');
  AddDelphiPattern(DELPHI_VERSION_8, '80');
  AddDelphiPattern(DELPHI_VERSION_8, 'NET');
  AddDelphiPattern(DELPHI_VERSION_8, '_8');
  AddDelphiPattern(DELPHI_VERSION_8, '8');

  AddDelphiPattern(DELPHI_VERSION_2005, 'delphi2005');
  AddDelphiPattern(DELPHI_VERSION_2005, '2005');
  AddDelphiPattern(DELPHI_VERSION_2005, '_90');
  AddDelphiPattern(DELPHI_VERSION_2005, '_9');
  AddDelphiPattern(DELPHI_VERSION_2005, 'r90');
  AddDelphiPattern(DELPHI_VERSION_2005, 'd90');
  AddDelphiPattern(DELPHI_VERSION_2005, '90');
  AddDelphiPattern(DELPHI_VERSION_2005, 'r9');
  AddDelphiPattern(DELPHI_VERSION_2005, 'd9');
  AddDelphiPattern(DELPHI_VERSION_2005, '9');

  AddDelphiPattern(DELPHI_VERSION_2006, 'delphi2006');
  AddDelphiPattern(DELPHI_VERSION_2006, 'd2006');
  AddDelphiPattern(DELPHI_VERSION_2006, 'rs10');
  AddDelphiPattern(DELPHI_VERSION_2006, '2006');
  AddDelphiPattern(DELPHI_VERSION_2006, '_100');
  AddDelphiPattern(DELPHI_VERSION_2006, '_10');
  AddDelphiPattern(DELPHI_VERSION_2006, 'r100');
  AddDelphiPattern(DELPHI_VERSION_2006, 'd100');
  AddDelphiPattern(DELPHI_VERSION_2006, '100');
  AddDelphiPattern(DELPHI_VERSION_2006, 'r10');
  AddDelphiPattern(DELPHI_VERSION_2006, 'd10');
  AddDelphiPattern(DELPHI_VERSION_2006, '10');

  AddDelphiPattern(DELPHI_VERSION_2007, 'delphi2007');
  AddDelphiPattern(DELPHI_VERSION_2007, 'd2007');
  AddDelphiPattern(DELPHI_VERSION_2007, 'rs11');
  AddDelphiPattern(DELPHI_VERSION_2007, '2007');
  AddDelphiPattern(DELPHI_VERSION_2007, '_110');
  AddDelphiPattern(DELPHI_VERSION_2007, 'r110');
  AddDelphiPattern(DELPHI_VERSION_2007, 'd110');
  AddDelphiPattern(DELPHI_VERSION_2007, '110');
  AddDelphiPattern(DELPHI_VERSION_2007, '_11');
  AddDelphiPattern(DELPHI_VERSION_2007, 'r11');
  AddDelphiPattern(DELPHI_VERSION_2007, 'd11');
  AddDelphiPattern(DELPHI_VERSION_2007, '11');

  AddDelphiPattern(DELPHI_VERSION_2009,'delphi2009');
  AddDelphiPattern(DELPHI_VERSION_2009,'d2009');
  AddDelphiPattern(DELPHI_VERSION_2009,'rs12');
  AddDelphiPattern(DELPHI_VERSION_2009,'2009');
  AddDelphiPattern(DELPHI_VERSION_2009,'_12');
  AddDelphiPattern(DELPHI_VERSION_2009,'r12');
  AddDelphiPattern(DELPHI_VERSION_2009,'d12');
  AddDelphiPattern(DELPHI_VERSION_2009,'120');
  AddDelphiPattern(DELPHI_VERSION_2009,'12');

  AddDelphiPattern(DELPHI_VERSION_2010,'delphi2010');
  AddDelphiPattern(DELPHI_VERSION_2010,'d2010');
  AddDelphiPattern(DELPHI_VERSION_2010,'2010');
  AddDelphiPattern(DELPHI_VERSION_2010,'rs14');
  AddDelphiPattern(DELPHI_VERSION_2010,'rs13');
  AddDelphiPattern(DELPHI_VERSION_2010,'_14');
  AddDelphiPattern(DELPHI_VERSION_2010,'_13');
  AddDelphiPattern(DELPHI_VERSION_2010,'r14');
  AddDelphiPattern(DELPHI_VERSION_2010,'d14');
  AddDelphiPattern(DELPHI_VERSION_2010,'r13');
  AddDelphiPattern(DELPHI_VERSION_2010,'d13');
  AddDelphiPattern(DELPHI_VERSION_2010,'14');
  AddDelphiPattern(DELPHI_VERSION_2010,'13');

  AddDelphiPattern(DELPHI_VERSION_XE1,'delphixe');
  AddDelphiPattern(DELPHI_VERSION_XE1,'RS15');
  AddDelphiPattern(DELPHI_VERSION_XE1,'dxe');
  AddDelphiPattern(DELPHI_VERSION_XE1,'xe');
  AddDelphiPattern(DELPHI_VERSION_XE1,'150');
  AddDelphiPattern(DELPHI_VERSION_XE1,'_15');
  AddDelphiPattern(DELPHI_VERSION_XE1,'r15');
  AddDelphiPattern(DELPHI_VERSION_XE1,'d15');
  AddDelphiPattern(DELPHI_VERSION_XE1,'15');

  AddDelphiPattern(DELPHI_VERSION_XE2,'delphixe2');
  AddDelphiPattern(DELPHI_VERSION_XE2,'RS16');
  AddDelphiPattern(DELPHI_VERSION_XE2,'dxe2');
  AddDelphiPattern(DELPHI_VERSION_XE2,'xe2');
  AddDelphiPattern(DELPHI_VERSION_XE2,'160');
  AddDelphiPattern(DELPHI_VERSION_XE2,'_16');
  AddDelphiPattern(DELPHI_VERSION_XE2,'r16');
  AddDelphiPattern(DELPHI_VERSION_XE2,'d16');
  AddDelphiPattern(DELPHI_VERSION_XE2,'16');

  AddDelphiPattern(DELPHI_VERSION_XE3,'delphixe3');
  AddDelphiPattern(DELPHI_VERSION_XE3,'RS17');
  AddDelphiPattern(DELPHI_VERSION_XE3,'dxe3');
  AddDelphiPattern(DELPHI_VERSION_XE3,'xe3');
  AddDelphiPattern(DELPHI_VERSION_XE3,'170');
  AddDelphiPattern(DELPHI_VERSION_XE3,'_17');
  AddDelphiPattern(DELPHI_VERSION_XE3,'r17');
  AddDelphiPattern(DELPHI_VERSION_XE3,'d17');
  AddDelphiPattern(DELPHI_VERSION_XE3,'17');

  AddDelphiPattern(DELPHI_VERSION_XE4,'delphiXE4');
  AddDelphiPattern(DELPHI_VERSION_XE4,'RS18');
  AddDelphiPattern(DELPHI_VERSION_XE4,'dXE4');
  AddDelphiPattern(DELPHI_VERSION_XE4,'XE4');
  AddDelphiPattern(DELPHI_VERSION_XE4,'180');
  AddDelphiPattern(DELPHI_VERSION_XE4,'_18');
  AddDelphiPattern(DELPHI_VERSION_XE4,'r18');
  AddDelphiPattern(DELPHI_VERSION_XE4,'d18');
  AddDelphiPattern(DELPHI_VERSION_XE4,'18');

  AddDelphiPattern(DELPHI_VERSION_XE5,'delphiXE5');
  AddDelphiPattern(DELPHI_VERSION_XE5,'RS19');
  AddDelphiPattern(DELPHI_VERSION_XE5,'dXE5');
  AddDelphiPattern(DELPHI_VERSION_XE5,'XE5');
  AddDelphiPattern(DELPHI_VERSION_XE5,'190');
  AddDelphiPattern(DELPHI_VERSION_XE5,'_19');
  AddDelphiPattern(DELPHI_VERSION_XE5,'r19');
  AddDelphiPattern(DELPHI_VERSION_XE5,'d19');
  AddDelphiPattern(DELPHI_VERSION_XE5,'19');

  AddDelphiPattern(DELPHI_VERSION_XE6,'delphiXE6');
  AddDelphiPattern(DELPHI_VERSION_XE6,'RS20');
  AddDelphiPattern(DELPHI_VERSION_XE6,'dXE6');
  AddDelphiPattern(DELPHI_VERSION_XE6,'XE6');
  AddDelphiPattern(DELPHI_VERSION_XE6,'200');
  AddDelphiPattern(DELPHI_VERSION_XE6,'_20');
  AddDelphiPattern(DELPHI_VERSION_XE6,'r20');
  AddDelphiPattern(DELPHI_VERSION_XE6,'d20');
  AddDelphiPattern(DELPHI_VERSION_XE6,'20');

finalization
  for I := DELPHI_VERSION_MIN to DELPHI_VERSION_MAX do
    patterns[I].Free;
end.
