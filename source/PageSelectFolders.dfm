inherited SelectFoldersPage: TSelectFoldersPage
  Left = 346
  Top = 221
  ActiveControl = edtBaseFolder
  Caption = 'SelectFoldersPage'
  ClientHeight = 289
  ClientWidth = 492
  Constraints.MinHeight = 300
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 508
  ExplicitHeight = 328
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 64
    Top = 250
    Width = 378
    Height = 51
    Anchors = [akLeft, akTop, akRight]
    Caption = 
      '     the  source files must place in '#39'source'#39' folder or '#39'sources' +
      #39' folder  while package files  place in '#39'packages'#39' folder'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -14
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object Label5: TLabel
    Left = 46
    Top = 225
    Width = 73
    Height = 19
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Warning!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object grpPackagePattern: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 134
    Width = 486
    Height = 79
    Align = alTop
    Caption = 'Pattern to select package files '
    TabOrder = 2
    DesignSize = (
      486
      79)
    object Label3: TLabel
      Left = 12
      Top = 51
      Width = 98
      Height = 13
      Caption = 'Package File Pattern'
    end
    object Label2: TLabel
      Left = 15
      Top = 16
      Width = 408
      Height = 26
      Anchors = [akLeft, akTop, akRight]
      Caption = 
        'Specify a pattern that matches for the package files that are su' +
        'itable for your delphi installation. ie: *d7.dpk for Delphi 7'
      WordWrap = True
    end
    object cbPattern: TComboBox
      Left = 116
      Top = 48
      Width = 360
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ImeName = #20013#25991'('#31616#20307') - '#26497#28857#20116#31508
      TabOrder = 0
      Text = '*.dpk'
    end
  end
  object grpBaseFolder: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 486
    Height = 65
    Align = alTop
    Caption = 'Select Base Folder contains both Package and Source files'
    TabOrder = 0
    DesignSize = (
      486
      65)
    object Label1: TLabel
      Left = 12
      Top = 16
      Width = 56
      Height = 13
      Caption = 'Base Folder'
    end
    object btnSelectFolder: TButton
      Left = 444
      Top = 35
      Width = 32
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 0
      WordWrap = True
      OnClick = btnSelectFolderClick
    end
    object edtBaseFolder: TEdit
      Left = 12
      Top = 35
      Width = 426
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ImeName = #20013#25991'('#31616#20307') - '#26497#28857#20116#31508
      TabOrder = 1
      OnChange = edtBaseFolderChange
      OnDblClick = edtBaseFolderDblClick
    end
  end
  object grpDelphiVersion: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 74
    Width = 486
    Height = 54
    Align = alTop
    Caption = 'Installed Delphi Versions'
    TabOrder = 1
    DesignSize = (
      486
      54)
    object cbDelphiVersions: TComboBox
      Left = 15
      Top = 20
      Width = 451
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      ImeName = #20013#25991'('#31616#20307') - '#26497#28857#20116#31508
      TabOrder = 0
      OnChange = cbDelphiVersionsChange
    end
  end
end
