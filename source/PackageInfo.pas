{**
 DelphiPI (Delphi Package Installer)
 Author  : ibrahim dursun (ibrahimdursun gmail)
 License : GNU General Public License 2.0
**}
unit PackageInfo;
interface
uses  Classes, StrUtils, TreeModel,JclFileUtils;

type
  TPackageStatus = (psNone, psCompiling, psReCompiling, psInstalling,
    psSuccess, psError);
  TPackageInfo = class
  private
    fRequiredPackageList: TStringList;
    fContainedFileList : TStringList;
    fSourceFilePaths   : TStringList;
    FDescription : String;
    FSuffixs : TStringList;
    FPackageName : String;
    fFileName: String;
    fStatus: TPackageStatus;
    fInxMajor : Integer;//major index
    fInxMinor : Integer;
    FDesignOnly: Boolean;
    function GetBPLName(Path:string): string;
    function GetSuffix(index: Integer): string;
    function getRunOnly: Boolean;//minor index
  public
    constructor Create;overload;
    constructor Create(const packageName:string);overload;
    destructor Destroy; override;
    function DependsOn(const package: TPackageInfo): Boolean; overload;
    procedure AddSuffix(Suffix:string);
    property Description: string read FDescription write FDescription;
    property PackageName: string read FPackageName write FPackageName;
    property RunOnly: Boolean read getRunOnly;
    property Suffixs[index:Integer]: string read GetSuffix;
    property RequiredPackageList: TStringList read fRequiredPackageList;
    property ContainedFileList: TStringList read fContainedFileList;
    property SourceFilePaths: TStringList read fSourceFilePaths;
    property BPLName[Path:string]:string read GetBPLName;
    property FileName: string read fFileName write fFileName;
    property Status: TPackageStatus read fStatus write fStatus;

    property DesignOnly: Boolean read FDesignOnly write FDesignOnly;//2014.7.19 Rishunxue
    property InxMajor: Integer read fInxMajor write fInxMajor;
    property InxMinor: Integer read fInxMinor write fInxMinor;
  end;

implementation
uses  SysUtils, contnrs, JclStrings;

constructor TPackageInfo.Create;
begin
  fInxMajor:=0;
  fInxMinor:=0;
  fRequiredPackageList := TStringList.Create;
  fContainedFileList := TStringList.Create;
  fSourceFilePaths := TStringList.Create;
  fSuffixs := TStringList.Create;
end;

destructor TPackageInfo.Destroy;
begin
  FreeAndNil(fRequiredPackageList);
  FreeAndNil(fContainedFileList);
  inherited;
end;

procedure TPackageInfo.AddSuffix(Suffix: string);
begin
 self.FSuffixs.Add(Suffix);
end;

function TPackageInfo.GetBPLName(Path:string): string;
var
 sName:string;
begin
 if fSuffixs.Count=0 then
 begin
  result:=PathAddSeparator(Path)+PathExtractFileNameNoExt(FileName)+'.bpl';
  exit;
 end;
 for sName in fSuffixs do begin
   result:=PathAddSeparator(Path)+PathExtractFileNameNoExt(FileName)
         + sName + '.bpl';
   if FileExists(result) then break;
   result:='';
 end;
end;

function TPackageInfo.getRunOnly: Boolean;
begin
 result:=not DesignOnly;
end;

function TPackageInfo.GetSuffix(index: Integer): string;
begin
 if (index>=0)and (index<fSuffixs.Count)
  then result:=fSuffixs[index]
  else result:='';
end;

constructor TPackageInfo.Create(const packageName: string);
begin
  Create;
  FPackageName := packageName;
end;

function TPackageInfo.DependsOn(const package: TPackageInfo): Boolean;
begin
  assert(package <> nil);
  Result := RequiredPackageList.IndexOf(package.packageName) > -1;
end;


end.

