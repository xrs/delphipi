inherited SelectCompilerOptions: TSelectCompilerOptions
  Left = 290
  Top = 283
  Caption = 'SelectCompilerOptions'
  ClientHeight = 288
  ClientWidth = 492
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 508
  ExplicitHeight = 327
  PixelsPerInch = 96
  TextHeight = 13
  object grpOutputFolders: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 486
    Height = 190
    Align = alTop
    Caption = 'Compiler Options'
    TabOrder = 0
    DesignSize = (
      486
      190)
    object lblBPLOutputFolder: TLabel
      Left = 15
      Top = 27
      Width = 91
      Height = 13
      Caption = 'BPL Output Folder:'
    end
    object lblDCP: TLabel
      Left = 15
      Top = 56
      Width = 94
      Height = 13
      Caption = 'DCP Output Folder:'
    end
    object lblDCU: TLabel
      Left = 15
      Top = 83
      Width = 95
      Height = 13
      Caption = 'DCU Output Folder:'
    end
    object Label1: TLabel
      Left = 15
      Top = 110
      Width = 95
      Height = 13
      Caption = 'Compiler Directives:'
    end
    object edtBPL: TEdit
      Left = 112
      Top = 24
      Width = 320
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ImeName = #20013#25991'('#31616#20307') - '#26497#28857#20116#31508
      TabOrder = 0
      OnDblClick = edtBPLDblClick
      OnEnter = edtBPLEnter
      OnExit = edtBPLExit
    end
    object btnBPLBrowse: TButton
      Left = 438
      Top = 22
      Width = 28
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 1
      OnClick = btnBPLBrowseClick
    end
    object edtDCP: TEdit
      Left = 112
      Top = 53
      Width = 320
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ImeName = #20013#25991'('#31616#20307') - '#26497#28857#20116#31508
      TabOrder = 2
      OnDblClick = edtBPLDblClick
    end
    object btnDCPBrowse: TButton
      Left = 438
      Top = 51
      Width = 28
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 3
      OnClick = btnDCPBrowseClick
    end
    object edtDCU: TEdit
      Left = 112
      Top = 80
      Width = 320
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ImeName = #20013#25991'('#31616#20307') - '#26497#28857#20116#31508
      TabOrder = 4
      OnDblClick = edtBPLDblClick
    end
    object btnDCUBrowse: TButton
      Left = 438
      Top = 78
      Width = 28
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 5
      OnClick = btnDCUBrowseClick
    end
    object edtConditionals: TEdit
      Left = 112
      Top = 107
      Width = 354
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ImeName = #20013#25991'('#31616#20307') - '#26497#28857#20116#31508
      TabOrder = 6
      OnDblClick = edtBPLDblClick
    end
    object rgMode: TRadioGroup
      Left = 112
      Top = 133
      Width = 354
      Height = 44
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Compile mode'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Compile and install'
        'Compile only'
        'Install Only')
      TabOrder = 7
    end
  end
  object rgPlatForm: TRadioGroup
    Left = 115
    Top = 199
    Width = 354
    Height = 42
    Anchors = [akLeft, akTop, akRight]
    Caption = 'PlatForm'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'WIN32'
      'WIN64'
      'OSX32')
    TabOrder = 1
  end
end
