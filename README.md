DelphiPI
========

DELPHI 组件包安装器


DelphiPI是个帮助你安装DELPHI组件的工具。DelphiPI自动分析包之间的依赖关系后并完成组件的编译、安装。 

###编译DelphiPI的方法

DelphiPI需要两个开源包才能编译：

  * JCL(https://github.com/project-jedi/jcl).当前使用的版本为jcl-2.6.0.5178
  * Virtual TreeView(www.soft-gems.net). 安装方法请参阅INSTALL.txt文件.版本为V5.41
